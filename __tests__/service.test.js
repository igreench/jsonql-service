import JsonQLService from '../src';

describe('JsonQL Service', () => {
  const peopleService = new JsonQLService([
    {
      id: 0,
      name: 'Peter',
    },
    {
      id: 1,
      name: 'John',
    },
  ]);

  const projectsService = new JsonQLService([
    {
      id: 0,
      name: 'Project0',
    },
    {
      id: 1,
      name: 'Project1',
    },
  ]);

  const projectsParticipationService = new JsonQLService([
    {
      id: 0,
      project_id: 0,
      person_id: 0,
    },
    {
      id: 1,
      project_id: 1,
      person_id: 1,
    },
  ]);

  const requestsService = new JsonQLService([
    {
      id: 0,
      requester_id: 0,
      requester_type: 0,
      responder_id: 0,
      responder_type: 1,
      status: 0,
    },
    {
      id: 1,
      requester_id: 1,
      requester_type: 1,
      responder_id: 1,
      responder_type: 0,
      status: 1,
    },
  ]);

  const dataService = {
    people: peopleService,
    projects: projectsService,
    projectsParticipation: projectsParticipationService,
    requests: requestsService,
  };

  peopleService.setRootService(dataService);
  projectsService.setRootService(dataService);
  projectsParticipationService.setRootService(dataService);
  requestsService.setRootService(dataService);

  test('Getting simple', () => {
    const x = dataService.people.get({});

    // assert(x.length === 2);

    expect(x.length).toBe(2);
  });

  test('Getting complex', () => {
    const x = dataService.people.get({
      query: {
        projectsParticipation: {
          from: 'projectsParticipation',
          by: 'person_id',
          orig: 'id',
          as: 'projectsParticipation',
          query: {
            project: {
              from: 'projects',
              by: 'id',
              orig: 'project_id',
              isOne: true,
            },
          },
        },
      },
    });

    expect(x[1].projectsParticipation[0].project.name).toBe('Project1');
  });

  test('Adding', () => {
    const x = dataService.projects.add({
      name: 'test',
    });

    expect(dataService.projects.items[dataService.projects.items.length - 1].name).toBe('test');

    expect(x.id).toBe(100);
  });
});
