export default class JsonQLService {
  constructor(items) {
    this.items = items;
    this.currentId = 100;
    this.rootService = null;
    this.isDebug = process.env.NODE_ENV !== 'production';
  }

  setRootService(service) {
    this.rootService = service;
  }

  getById(id) {
    const items = this.items.filter((el) => el.id === id);
    if (items.length && items.length > 0) {
      return items[0];
    }
    return null;
  }

  get({
    filter = null, query = null, isOne = false, pagination = null,
  }) {
    let items = null;
    if (filter) {
      items = this.items.filter((el) => Object.keys(filter)
        .filter((key) => String(el[key]) !== String(filter[key])).length === 0);
    } else {
      items = this.items;
    }
    if (!query) {
      if (isOne) {
        return items.length > 0 ? items[0] : null;
      }
      return items;
    }
    Object.keys(query).forEach((key) => {
      if (!query[key].by || !query[key].orig) {
        return;
      }
      items = items.map((el) => {
        const name = query[key].as || key;
        if (!this.rootService) {
          if (this.isDebug) {
            console.error('[NNA warn]: Error in JsonQLService.get. "rootService" does not exist');
          }
          return {};
        }
        if (!this.rootService[query[key].from || key]) {
          if (this.isDebug) {
            console.error(`[NNA warn]: Error in JsonQLService.get. "rootService[key]" does not exist for query=${query}`);
          }
          return {};
        }
        const value = this.rootService[query[key].from || key].get({
          ...query[key],
          filter: {
            ...query[key].filter,
            [query[key].by]: el[query[key].orig],
          },
        });
        return { ...el, [name]: value };
      });
    });
    if (isOne) {
      return items.length > 0 ? items[0] : null;
    }
    if (pagination && pagination.count) {
      let offset = 0;
      if (pagination.offset) {
        offset = pagination.offset;
      }
      return items.slice(offset, pagination.count);
    }
    return items;
  }

  add(item) {
    this.items.push({ ...item, id: this.currentId });
    this.currentId += 1;
    return this.items[this.items.length - 1];
  }

  update(item) {
    const index = this.items.findIndex((el) => el.id === item.id);
    if (index === -1) {
      return null;
    }
    this.items[index] = { ...this.items[index], ...item };
    return this.items[index];
  }

  remove(item) {
    // TODO: mark as deleted
    const index = this.items.findIndex((el) => el.id === item.id);
    return index !== -1 ? this.items.splice(index, 1) : null;
  }
}
